annual_salary = float(input('Enter your annual salary:  '))
portion_down_payment = float(input('Enter percent of your of your salary to save as a decimal:   '))
total_cost = float(input('Enter the cost of your dream home:  '))
annual_semi_rise = float(input('Enter the semi annual raise, as a decimal:  '))

current_savings = 0
r = 0.04
months = 0
portion_saved = 0



while portion_saved < total_cost:
    annual_salary += annual_salary*(annual_semi_rise**2)
    portion_saved += (annual_salary * portion_down_payment)+(portion_saved * r / 12)
    months += 1

print("Enter number of months: ", months)